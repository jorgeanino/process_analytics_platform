# Product Management API

## Table of Contents

1. [Introduction](#introduction)
2. [Installation](#installation)
3. [Endpoints](#endpoints)
4. [Filters](#filters)
5. [Products](#products)
6. [License](#license)

## Introduction

This is the product management API which allows users to create, read, update, and delete products. With features for filtering.

## Installation

1. Clone this repository:
    ```bash
    git clone https://gitlab.com/jorgeanino/process_analytics_platform.git
    ```
2. Change to the project directory:
    ```bash
    cd process_analytics_platform
    ```
3. Run the following commands:
    ```bash
    chmod +x init_submodules.sh
    sudo sh init_submodules.sh
    ```
4. cd into platform-docker:
    ```bash
    cd process_analytics_platform
    ```
5. Before building and running the containers, make sure to set up your environment variables:
    ```bash
    cp .env.sample .env
    ```
6. Run containers:
    ```bash
    sudo sh run_containers.sh
    ```
7. To open project go into localhost:3000

## Endpoints

### 1. List Processes
- **Endpoint:** `/`
- **Method:** `GET`
- **Description:** Retrieves a list of processes annotated for outliers.

### 2. Import Processes from CSV
- **Endpoint:** `/import/`
- **Method:** `POST`
- **Description:** Upload and import process data from a CSV file. The expected format for the CSV includes columns:
  - Process ID
  - Name
  - Start Date
  - Start Time
  - End Date
  - End Time
  - Status
  - Message

  The Date and Time columns should adhere to the `%d/%m/%Y` and `%H:%M:%S` format.

### 3. Get Daily Process Data
- **Endpoint:** `/daily/`
- **Method:** `GET`
- **Description:** Fetches process data aggregated on a daily basis.

### 4. Get Monthly Process Data
- **Endpoint:** `/monthly/`
- **Method:** `GET`
- **Description:** Returns aggregated monthly process data, which includes total and error process counts for each month.

---

## Filters
The `ProcessViewSet` utilizes the `ProcessFilter` class, enabling filtering on the data. Apply these filters by appending them as query parameters to the request URL.

## Ordering
Ordering of returned processes is possible based on their names. Use the `ordering` query parameter and set its value to `name` for ascending order. For descending order, prepend with a `-`, i.e., `-name`.

## Pagination
The `Pagination` class handles the pagination of results. Configure items per page and other pagination settings within the `Pagination` class.

## Exceptions
- **Invalid File Type:** Uploading a non-CSV file results in a `400 Bad Request` with the error message "File type not supported."
- **Invalid Date Format:** If the CSV contains an incorrect date format, it will return a `400 Bad Request` with the error message "Invalid date format."

For successful CSV imports, expect a `201 Created` response, accompanied by the message: "Processes were imported successfully."

## License

This project is licensed under the MIT License. See [LICENSE](./LICENSE) for details.
